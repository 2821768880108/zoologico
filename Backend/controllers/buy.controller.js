'use strict'

var Buy = require('../models/buy.model');
var Product = require('../models/product.model');

//CREAR UNA COMPRA PARA ABASTECER LA EMPRESA DE UN PRODUCTO YA CREADO
function createBuy(req, res) {
    var buy = new Buy();
    var dates = req.body;

    if (dates.provider && dates.date && dates.nameProduct && dates.quantity && dates.priceUnit) {
        Buy.findOne({ date: dates.date, nameProduct: dates.nameProduct}, (err, buyFound) => {
            if (err) {
                res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
            } else if (buyFound) {
                res.send({ message: 'El producto ya se habastecio en la fecha estipulada' });
            } else {
                Product.findOne({ nameProduct: dates.nameProduct }, (err, productFound) => {
                    if (err) {
                        res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                    } else if (productFound) {

                        //var quantityProduct = Number(saleFound.quantity) + Number(dates.quantity);
                        buy.typeMovement = 'COMPRA';
                        buy.date = dates.date;
                        buy.provider = dates.provider;
                        buy.code = productFound.code;
                        buy.nameProduct = dates.nameProduct;
                        //buy.stock = quantityProduct;
                        buy.priceUnit = dates.priceUnit;
                        buy.quantity = dates.quantity;
                        buy.total = parseFloat(dates.priceUnit) * parseFloat(dates.quantity);

                        var priceV = Number(buy.priceUnit == 0)
                        if (priceV == true) {
                            res.status(500).send({ message: 'No puede agregar un precio con valor 0' })
                        } else if (priceV == false) {
                            var priceV2 = Number(buy.priceUnit < 0)                       
                            if(priceV2 == true){
                                res.status(500).send({ message: 'No puede agregar un precio con valor negativo' })
                            }else if (priceV2 == false){
                                var quantityV = Number(buy.quantity == 0)
                                if(quantityV == true){
                                    res.status(500).send({ message: 'No puede comprar una cantidad de producto con valor 0' })
                                }else if(quantityV == false){
                                    var quantityV2 = Number(buy.quantity < 0)                       
                                    if(quantityV2 == true){                                    
                                        res.status(500).send({ message: 'No puede comprar una cantidad de producto con valor negativo' })
                                }else if(quantityV2 == false){

                                 buy.save((err, buySaved) => {
                           if (err) {
                               res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                            } else if (buySaved) {
                        
                                Product.findOne({ nameProduct: buy.nameProduct }, (err, productFound) => {
                                    if (err) {
                                        res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                                    } else if (productFound) {
                                        var quantityProduct = Number(productFound.quantity + buy.quantity);
                                        Product.findOneAndUpdate({ nameProduct: buy.nameProduct }, { quantity: quantityProduct }, { new: true }, (err, quantityProductUpdated) => {
                                            if (err) {
                                                res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                                            } else if (quantityProductUpdated) {
                                                res.send({ message: 'La compra de ' + buySaved.nameProduct + ' se ha registrado correctamente', buySaved, quantityProductUpdated });
                                            } else {
                                                res.status(404).send({ message: 'No se ha podido actualizar la cantidad del producto, intente de nuevo' });
                                            }
                                        });
                                    } else {
                                        res.status(404).send({ message: 'No se ha encontrado el producto solicitado' });

                                    }
                                });
                                
                          } else {
                                res.status(404).send({ message: 'No se ha podido crear la compra correctamente' });
                           }
                        });
                                  
                                    
                                }

                                }
                            }
                        }
                      
                    }
                });

            }
        });
    } else {
        res.status(404).send({ message: 'Debe ingresar todos los datos necesarios' });
    }
}

//ELIMINAR UNA COMPRA
function removeBuy(req, res) {
    var idBuy = req.params.id

    Buy.findByIdAndRemove(idBuy, (err, buyRemoved) => {
        if (err) {
            res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
        } else if (buyRemoved) {
            res.send({ message: 'La compra de ' + buyRemoved.nameProduct + ' se a eliminado correctamente' });
        } else {
            res.status(404).send({ message: 'No se ha podido eliminar el producto' });
        }
    })

}

//LISTAR COMPRAS 
function listBuys(req, res) {

    Buy.find({}.exec, (err, buysList) => {
        if (err) {
            res.status(500).send({ message: 'Error al realizar la peticion, intente de nuevo' })
        } else if (buysList) {
            res.send({ message: 'Listado de productos:', buysList });
        } else {
            res.status(404).send({ message: 'Error al listar los Productos' });
        }
    });
}

//ACTUALIZAR COMPRA
function updateBuy(req, res) {
    var idBuy = req.params.id;
    var dates = req.body;

    Buy.findByIdAndUpdate(idBuy, dates, { new: true }, (err, buyUpdate) => {
        if (err) {
            res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
        } else if (buyUpdate) {
            res.send({ message: 'La compra del proveedor ' + buyUpdate.provider + ' se actualizo correctamente', buyUpdate });
        } else {
            res.status(404).send({ message: 'No se ha podido actualizar el producto' });
        }
    });
}

module.exports = {
    createBuy,
    removeBuy,
    listBuys,
    updateBuy,

}