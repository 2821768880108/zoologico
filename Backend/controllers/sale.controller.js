'use strict'

var Sale = require('../models/sale.model');
var Product = require('../models/product.model');

//CREAR UNA VENTA
function createSale(req, res) {
    var sale = new Sale();
    var dates = req.body;

    if (dates.client && dates.date && dates.nameProduct && dates.quantity) {

        Product.findOne({ nameProduct: dates.nameProduct }, (err, productFound) => {
            if (err) {
                res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
            } else if (productFound) {
                var priceProduct = productFound.priceUnit;
                //var quantityProduct = Number(saleFound.quantity) - Number(dates.quantity);
                sale.typeMovement = 'VENTA';
                sale.date = dates.date;
                sale.client = dates.client;
                sale.code = productFound.code;
                sale.nameProduct = dates.nameProduct;
                //sale.stock = quantityProduct;
                sale.priceUnit = priceProduct;
                sale.quantity = dates.quantity;
                sale.total = parseFloat(priceProduct) * parseFloat(dates.quantity);

                Product.findOne({ nameProduct: sale.nameProduct }, (err, productFound) => {
                    if (err) {
                        res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                    } else if (productFound) {
                        var cantidad = Number(productFound.quantity < sale.quantity)
                        var prueva = Number(productFound.quantity == 0)
                        if (cantidad == true || prueva == true) {
                            res.status(500).send({ message: 'No existe producto suficiente para realizar la venta' })
                        } else if (cantidad == false && prueva == false) {
                            var prueva2 = Number(sale.quantity < 0)
                            if (prueva2 == true) {
                                res.status(500).send({ message: 'No puede realizar una venta de producto con números negativos' })
                            } else if (prueva2 == false) {
                                var prueva3 = Number(sale.quantity == 0)
                                if (prueva3 == true) {
                                    res.status(500).send({ message: 'No puede realizar una venta de producto con cantidad 0' })
                                } else if (prueva3 == false) {
                                    sale.save((err, saleSaved) => {
                                        if (err) {
                                            res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                                        } else if (saleSaved) {
                                            var quantityProduct = Number(productFound.quantity - sale.quantity)
                                            Product.findOneAndUpdate({ nameProduct: sale.nameProduct }, { quantity: quantityProduct }, { new: true }, (err, quantityProductUpdated) => {
                                                if (err) {
                                                    res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                                                } else if (quantityProductUpdated) {

                                                    res.send({ message: 'La venta de ' + saleSaved.nameProduct + ' se ha registrado correctamente', saleSaved, quantityProductUpdated });
                                                } else {
                                                    res.status(404).send({ message: 'No se ha podido actualizar la cantidad del producto, intente de nuevo' });
                                                }
                                            });
                                        } else {
                                            res.status(404).send({ message: 'No se ha encontrado el producto solicitado' });
                                        }
                                    })
                                }

                            }


                        }
                    }
                })
            } else {
                res.status(404).send({ message: 'No se ha podido crear la venta correctamente' });
            }
        });
    } else {
        res.status(404).send({ message: 'Debe ingresar todos los datos necesarios' });
    }
}


//ELIMINAR UNA VENTA
function removeSale(req, res) {
    var idSale = req.params.id

    Sale.findByIdAndRemove(idSale, (err, saleRemoved) => {
        if (err) {
            res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
        } else if (saleRemoved) {
            res.send({ message: 'La venta del producto ' + saleRemoved.nameProduct + ' se a eliminado correctamente' });
        } else {
            res.status(404).send({ message: 'No se ha podido eliminar la venta del producto' });
        }
    })

}

//LISTAR TODAS LAS VENTAS
function listSales(req, res) {

    Sale.find({}.exec, (err, salesList) => {
        if (err) {
            res.status(500).send({ message: 'Error al realizar la peticion, intente de nuevo' })
        } else if (salesList) {
            res.send({ message: 'Listado de ventas:', salesList });
        } else {
            res.status(404).send({ message: 'Error al listar las ventas' });
        }
    });
}


//ACTUALIZAR UNA VENTA EN CASO DE DEVOLUCIÓN, ASI REGRESARA EL PRODUCTO AL STOCK
function updateSale(req, res) {
    var idSale = req.params.id;
    var dates = req.body;

    if (dates.client || dates.date || dates.nameProduct) {
        res.status(500).send({ message: 'No puede editar los siguientes datos: cliente, date, nameProduct' });
    } else if (dates.quantity == '') {
        res.status(500).send({ message: 'Debe crear un cambio' });
    }
    else {
        Sale.findById(idSale, (err, saleFound) => {
            if (err) {
                res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
            } else if (saleFound) {
                var quantityProductSale = saleFound.quantity;
                if (dates.quantity > quantityProductSale) {
                    res.send({ message: 'Debe realizar otra compra' })
                } else if (dates.quantity < quantityProductSale) {
                    Product.findOne({ nameProduct: saleFound.nameProduct }, (err, product1Found) => {
                        if (err) {
                            res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
                        } else if (product1Found) {
                            var priceProduct = quantityProductSale - dates.quantity;
                            var quantityProduct = Number(product1Found.quantity + priceProduct);
                            Product.findOneAndUpdate({ nameProduct: saleFound.nameProduct }, { quantity: quantityProduct }, { new: true }, (err, productUpdated) => {
                                if (err) {
                                    res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
                                } else if (productUpdated) {
                                    Sale.findByIdAndUpdate(idSale, { quantity: dates.quantity, total: Number(dates.quantity * productUpdated.priceUnit) }, { new: true }, (err, saleUpdate) => {
                                        if (err) {
                                            res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
                                        } else if (saleUpdate) {
                                            res.send({ message: 'La venta del producto ' + saleUpdate.nameProduct + ' se actualizo correctamente', saleUpdate });
                                        } else {
                                            res.status(404).send({ message: 'No se ha podido actualizar la venta del producto' });
                                        }
                                    });
                                } else {
                                    res.status(404).send({ message: 'No se ha encontrado el producto solicitado' });
                                }
                            })
                        } else {
                            res.status(404).send({ message: 'No se ha encontrado el producto solicitado' });
                        }
                    });
                }
            } else {
                res.status(404).send({ message: 'No se ha encontrado la venta solicitada' });
            }
        });
    }
}


module.exports = {
    createSale,
    removeSale,
    listSales,
    updateSale,

}