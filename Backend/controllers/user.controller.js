'use strict'

var bcrypt = require('bcrypt-nodejs');
var jwt = require('../services/jwt');
var User = require('../models/user.model');

//CREAR USUARIO ADMINISTRADOR
function saveUser(req, res){
    var user = new User();  
    var dates = req.body;


    if(dates.name && dates.lastname && dates.username && dates.email && dates.password){
        User.findOne({$or: [{username: dates.username}, {email: dates.email}]}, (err, userFound)=>{
            if(err){
                res.status(500).send({message: 'Error general en la peticion, intentelo mas tarde'})
            }else if(userFound){
                res.send({message: 'El nombre de usuario o email ya estan en uso'});
            }else{
                user.name = dates.name;
                user.lastname = dates.lastname;
                user.username = dates.username;
                user.email = dates.email;
                user.password = dates.password;
                user.role = 'ADMINISTRADOR'

                bcrypt.hash(dates.password, null, null, (err, passwordHash)=>{
                    if(err){
                        res.status(500).send({message: 'Error al encriptar contraseña'});
                    }else if(passwordHash){
                        user.password = passwordHash;

                        user.save((err, userSaved)=>{
                            if(err){
                                res.status(500).send({message: 'Error general en la peticion, intentelo mas tarde'}) 
                            }else if(userSaved){
                                res.send({message: 'Usuario de rol ' + userSaved.role  + ' registrado correctamente', userSaved});
                            }else{
                                res.status(404).send({message: 'No se ha podido crear el usuario correctamente'});
                            }
                        });
                    }else{
                        res.status(404).send({message: 'Error inesperado'})
                    }
                });
            }
        });
    }else{
        res.status(404).send({message: 'Debe ingresar todos los datos necesarios'});
    }
}

//USUARIOS LOS CUALES SOLO PUEDEN SER CREADOS POR EL ADMINISTRADOR
function createUser(req, res){
    var user = new User();  
    var dates = req.body;

    if (!req.headers.authorization) {
        res.status(403).send({message: 'No tiene permiso para realizar esta accion'})
    }else{
        
        if(dates.name && dates.lastname && dates.username && dates.email && dates.password){
            User.findOne({$or: [{username: dates.username}, {email: dates.email}]}, (err, userFound)=>{
                if(err){
                    res.status(500).send({message: 'Error general en la peticion, intentelo mas tarde'})
                }else if(userFound){
                    res.send({message: 'El nombre de usuario o email ya estan en uso'});
                }else{
                    user.name = dates.name;
                    user.lastname = dates.lastname;
                    user.username = dates.username;
                    user.email = dates.email;
                    user.password = dates.password;
                    user.role = dates.role;

                    bcrypt.hash(dates.password, null, null, (err, passwordHash)=>{
                        if(err){
                            res.status(500).send({message: 'Error al encriptar contraseña'});
                        }else if(passwordHash){
                            user.password = passwordHash;

                            user.save((err, userSaved)=>{
                                if(err){
                                    res.status(500).send({message: 'Error general en la peticion, intentelo mas tarde'}) 
                                }else if(userSaved){
                                    res.send({message: 'Usuario de rol ' + userSaved.role  + ' registrado correctamente', userSaved});
                                }else{
                                    res.status(404).send({message: 'No se ha podido crear el usuario correctamente'});
                                }
                            });
                        }else{
                            res.status(404).send({message: 'Error inesperado'})
                        }
                    });
                }
            });
        }else{
        res.status(404).send({message: 'Debe ingresar todos los datos necesarios'});
        }
    }
}

//ELIMINAR USUARIO SOLO ADMINISTRADOR LO PUEDE REALIZAR
function removeUser(req, res){
    var idUser = req.params.id

    if (!req.headers.authorization) {
        res.status(403).send({message: 'No tiene permisos para realizar esta accion'});
    }else{
        User.findByIdAndRemove(idUser, (err, userRemoved)=>{
            if(err){
                res.status(500).send({message: 'Error general al realizar la peticion, intente de nuevo'});
            }else if(userRemoved){
                res.send({message: 'Usuario ' + userRemoved.name + ' ' +  userRemoved.lastname + ' Eliminado correctamente'});
            }else{
                res.status(404).send({message: 'No se ha podido eliminar el usuario'});
            }
        })
    }

}
    
//LISTAR TODOS LOS USUARIOS SOLO ADMINISTRADOR LO PUEDE REALIZAR
function getUsers(req, res){
    if (!req.headers.authorization) {
        res.status(403).send({message: 'No tiene permisos para realizar esta accion'});
    }else{
        User.find({}.exec, (err, usersGotten)=>{
        if(err){
            res.status(500).send({message: 'Error general al realizar la peticion, intente de nuevo'});
        }else if(usersGotten){
            res.status(200).send({message: 'Listado de usuarios', usersGotten})
        }else{
            res.status(404).send({message: 'No se han podido listar los usuarios existentes'});
        }
    })
}
}

//ACTUALIZAR USUARIOS ADMINISTRADOR LO PUEDE REALIZAR
function updateUser(req, res){
    var idUser = req.params.id;
    var dates = req.body;

    if (!req.headers.authorization) {
        res.status(403).send({message: 'No tienes derechos para realizar esta accion'});
    }else{
            User.findByIdAndUpdate(idUser, dates, {new: true}, (err, userUpdated)=>{
                if(err){
                    res.status(500).send({message: 'Error general al realizar la peticion, intente de nuevo'});
                }else if(userUpdated){
                    res.send({message: 'Usuario ' + userUpdated.name + ' ' + userUpdated.lastname + ' Actualizado correctamente', userUpdated});
                }else{
                    res.status(404).send({message: 'No se ha podido actualizar el usuario'});
                }
            });
        }
}

//INICIAR SECIÓN
function login(req, res){
    var dates = req.body;

    if(dates.username){
        if(dates.password){
            User.findOne({username: dates.username}, (err, check)=>{
                    if(err){
                        res.status(500).send({message: 'Error general'});
                    }else if(check){
                        bcrypt.compare(dates.password, check.password, (err, passworOk)=>{
                            if(err){
                                res.status(500).send({message: 'Error al comparar'});
                            }else if(passworOk){
                                if(dates.gettoken = true){
                                    res.send({token: jwt.createToken(check)});
                                }else{
                                    res.send({message: 'Bienvenido',user:check});
                                }
                            }else{
                                res.send({message: 'Contraseña incorrecta'});
                            }
                        });
                    }else{
                        res.send({message: 'Datos de usuario incorrectos'});
                    }
                });
        }else{
           res.send({message: 'Ingresa tu contraseña'}); 
        }
    }else{
        res.send({message: 'Ingresa tu correo o tu username'});
    }
}


module.exports = {
    saveUser,
    createUser,
    getUsers,
    updateUser,
    login,
    removeUser
}