'use strict'

var Inventary = require('../models/inventary.model');
var Buy = require('../models/buy.model');
var Sale = require('../models/sale.model');
var Product = require('../models/product.model');
var pdf = require('pdfkit');
var fs = require('fs');
var productXlsx = require('mongo-xlsx');

function saveInventary(req, res) {
    var inventary = new Inventary();
    var dates = req.body;


    if (dates.numberInventary && dates.nameInventary && dates.dateInit && dates.dateEnd) {
        Inventary.findOne({ $or: [{ numberInventary: dates.numberInventary }, { nameInventary: dates.nameInventary }] }, (err, inventaryFound) => {
            if (err) {
                res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
            } else if (inventaryFound) {
                res.send({ message: 'Este inventario ya existe' });
            } else {
                inventary.numberInventary = dates.numberInventary;
                inventary.nameInventary = dates.nameInventary;
                inventary.dateInit = dates.dateInit;
                inventary.dateEnd = dates.dateEnd;
               
                Buy.find({date: {$gte: dates.dateInit, $lte: dates.dateEnd}}, (err, buyFound)=>{
                    inventary.buyInventary = buyFound;
                    if(err){
                        res.status(500).send({ message: 'Error al realizar la peticion' });
                    }else if(buyFound){
                        Sale.find({date: {$gte: dates.dateInit, $lte: dates.dateEnd}}, (err, salesFound)=>{
                            inventary.saleInventary = salesFound;
                            if(err){
                                res.status(500).send({ message: 'Error al realizar la peticion' });
                            }else if(salesFound){      
                                inventary.save((err, inventarySaved) => {
                                    if (err) {
                                        res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                                    } else if (inventarySaved) {
                                        res.send({ message: 'El inventario número: ' + inventarySaved.numberInventary + ' con el nombre de: ' + inventarySaved.nameInventary +' se ha registrado correctamente', inventarySaved});
                                    } else {
                                        res.status(404).send({ message: 'No se ha podido crear el inventario correctamente' });
                                    }
                                });
                            }else{res.status(404).send({ message: 'No se ha encontrado ventas' }); 
                        }
                    })
                }else{ 
                    res.status(404).send({ message: 'No se ha encontrado compras' }); 
                }
            })           
            }
        });
    } else {
        res.status(404).send({ message: 'Debe ingresar todos los datos necesarios' });
    }
}


/*
function  setContact(req, res){
    let userId = req.params.id;
    let paramsContact = req.body;
    let contact = new Contact();

    User.findById(userId, (err, userOk)=>{
        if(err){
            res.status(500).send({message: 'Error general'});
        }else if(userOk){
            if(paramsContact.name && paramsContact.phone){
                contact.name = paramsContact.name;
                contact.lastname = paramsContact.lastname;
                contact.phone = paramsContact.phone;

                User.findByIdAndUpdate(userId, {$push:{contacts: contact}}, {new:true},(err, userUpdated)=>{
                    if(err){
                        res.status(500).send({message: 'Error general'});
                    }else if(userUpdated){
                        res.send({user: userUpdated});
                    }else{
                        res.status(404).send({message: 'Usuario no actualizado'});
                    }
                });
            }else{
                res.status(200).send({message: 'Ingese los datos minimos para agregar un contacto'});
            }
        }else{
            res.status(404).send({message: 'Usuario inexistente'});
        }
    });
}*/

module.exports = {
    saveInventary
}