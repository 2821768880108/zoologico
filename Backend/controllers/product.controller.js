'use strict'

var Product = require('../models/product.model');
var Buy = require('../models/buy.model');
var Sale = require('../models/sale.model');
var pdf = require('pdfkit');
var fs = require('fs');
var productXlsx = require('mongo-xlsx');

//CREAR UN PRODUCTO 
function createProduct(req, res) {
    var product = new Product();
    var dates = req.body;

    if (dates.code && dates.nameProduct && dates.category && dates.priceUnit) {
        Product.findOne({ $or: [{ code: dates.code }, { nameProduct: dates.nameProduct }] }, (err, productFound) => {
            if (err) {
                res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
            } else if (productFound) {
                res.send({ message: 'El codigo o el producto que desea crear ya existe' });
            } else {
                product.code = dates.code;
                product.nameProduct = dates.nameProduct;
                product.category = dates.category;
                product.priceUnit = dates.priceUnit;
                product.quantity = dates.quantity;

                product.save((err, productSaved) => {
                    if (err) {
                        res.status(500).send({ message: 'Error general en la peticion, intentelo mas tarde' })
                    } else if (productSaved) {
                        res.send({ message: 'El producto ' + productSaved.nameProduct + ' se ha registrado correctamente', productSaved });
                    } else {
                        res.status(404).send({ message: 'No se ha podido crear el producto correctamente' });
                    }
                });
            }
        });
    } else {
        res.status(404).send({ message: 'Debe ingresar todos los datos necesarios' });
    }
}

//ELIMINAR PRODUCTO
function removeProduct(req, res) {
    var idProduct = req.params.id

    Product.findByIdAndRemove(idProduct, (err, productRemoved) => {
        if (err) {
            res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
        } else if (productRemoved) {
            res.send({ message: 'El producto ' + productRemoved.nameProduct + ' Eliminado correctamente' });
        } else {
            res.status(404).send({ message: 'No se ha podido eliminar el producto' });
        }
    })

}

//LISTAR PRODUCTOS
function listProducts(req, res) {

    Product.find({}.exec, (err, productsList) => {
        if (err) {
            res.status(500).send({ message: 'Error al realizar la peticion, intente de nuevo' })
        } else if (productsList) {
            res.send({ message: 'Listado de productos:', productsList });
        } else {
            res.status(404).send({ message: 'Error al listar los Productos' });
        }
    })
}

//ACTUALIZAR PRODUCTOS
function updateProduct(req, res) {
    var idProduct = req.params.id;
    var dates = req.body;

        Product.findByIdAndUpdate(idProduct, dates, { new: true }, (err, productUpdate) => {
            if (err) {
                res.status(500).send({ message: 'Error general al realizar la peticion, intente de nuevo' });
            } else if (productUpdate) {
                res.send({ message: 'El producto ' + 'de categoria ' + productUpdate.category + ' se actualizo correctamente', productUpdate });
            } else {
                res.status(404).send({ message: 'No se ha podido actualizar el producto' });
            }
        });
}

//BUSCAR PRODUCTO
function findProduct(req, res) {
    var idProduct = req.params.id;

    Product.findById(idProduct, (err, productFound) => {
        if (err) {
            res.status(500).send({ message: 'Error al realizar la peticion' });
        } else if (productFound) {
            res.send({ message: 'Producto encontrado', productFound });
        } else {
            res.status(404).send({ message: 'No se ha encontrado el producto' });

        }
    })
}

//BUSCAR PRODUCTO POR NOMBRE
function findProductbyname(req, res) {
    var dates = req.body;
    var nameProduct = dates.nameProduct;

    Product.findOne({ $or: [{ "nameProduct": { $regex: "^" + dates.nameProduct, $options: "i" } }] }, (err, productFind) => {
        if (err) {
            res.status(500).send({ message: 'Error al realizar la peticion' });
        } else if (productFind) {
            res.send({ message: 'Producto encontrado', productFind });
        } else {
            res.status(404).send({ message: 'No se ha encontrado el producto' });
        }
    });
}

//BUSCAR PRODUCTOS SIN EXISTENCIA
function findProducoutofstock(req, res) {

    Product.find({ quentity:0 }, (err, productFound) => {
        if (err) {
            res.status(500).send({ message: 'Error al realizar la peticion' });
        } else if (productFound) {
            res.send({ message: 'Productos sin existencia encontrados', productFound });
        } else {
            res.status(404).send({ message: 'No se ha encontrado el producto' });
        }
    })
}


//VER INVENTARIO DE UN DETERMINADO PORDUCTO
function viewPBS(req, res){
    var dateInit = req.body.dateInit;
    var dateEnd = req.body.dateEnd;
    var nameProduct = req.body.nameProduct;

    Buy.find({date: {$gte: dateInit, $lte: dateEnd}}, (err, buyFound)=>{
        if(err){
            res.status(500).send({ message: 'Error al realizar la peticion' });
        }else if(buyFound){
            Sale.find({date: {$gte: dateInit, $lte: dateEnd}}, (err, salesFound)=>{
                if(err){
                    res.status(500).send({ message: 'Error al realizar la peticion' });
                }else if(salesFound){      
                res.send({/*message: 'Informacion sobre compra de producto', buyFound, message2: 'Inventario de Producto actual', productFound,*/ message2: 'Listado de compras y ventas del dia ' + dateInit + ' al dia ' + dateEnd, buyFound, salesFound});
                }else{
                    res.status(404).send({ message: 'No se ha encontrado ventas' }); 
                }
            })
        }else{ 
            res.status(404).send({ message: 'No se ha encontrado compras' }); 
        }
    })
}


//GENERAR UN EXCEL SOBRE TODOS LOS PRODUCTOS EXISTENTES
function dataExcel (req,res){

    Product.find((err,product)=>{
        if(err){
            res.status(500).send({ message : 'Error general en el servidor'});
        } else if (product){
            var model = productXlsx.buildDynamicModel(product);

            productXlsx.mongoData2Xlsx(product,model,(err,exitoso)=>{
                if(err){
                    res.status(500).send({message : 'Error general en el servidor'});
                } else if(exitoso) {
                    res.send({ message : 'Excel generado exitoso'});
                }
            })
        } else {
            res.status(404).send({ message : 'No hay datos para generar el archivo Excel'});
        }
    })
}


module.exports = {
    createProduct,
    removeProduct,
    listProducts,
    updateProduct,
    findProduct,
    findProductbyname,
    findProducoutofstock,
    viewPBS,
    dataExcel,
}