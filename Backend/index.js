'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = 3800;

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://127.0.0.1:27017/Inventory', {useNewUrlParser: true, useUnifiedTopology:true, useFindAndModify:false})
    .then(()=>{
        console.log('La conexión a la base de datos ha sido exitosa');
        app.listen(port, ()=>{
            console.log('El servidor de express esta corriendo sin problemas en el puerto:', port);
        });
    }).catch(err =>{
        console.log('No se ha podido establecer la conexión con la base de datos', err);
        
    });
