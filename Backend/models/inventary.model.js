'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var inventorySchema = Schema({
    numberInventary: Number,
    nameInventary: String,
    dateInit: Date,
    dateEnd: Date,
    buyInventary: [],
    saleInventary: []
});

module.exports = mongoose.model('inventory', inventorySchema);