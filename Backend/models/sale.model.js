'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var saleSchema = Schema({
    typeMovement: String,
    date: Date,
    client: String,
    code: Number,
    nameProduct: String,
    stock: Number,
    priceUnit: String,
    quantity: Number,
    total:Number
});

module.exports = mongoose.model('sale', saleSchema);