'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = Schema({
    code: Number,
    nameProduct: String,
    category: String,
    priceUnit: String,
    quantity: Number
});

module.exports = mongoose.model('product', productSchema);