'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var buySchema = Schema({
    typeMovement: String,
    date: Date,
    provider: String,
    code: Number,
    nameProduct: String,
    stock: Number,
    quantity: Number,
    priceUnit: String,
    total:Number
});

module.exports = mongoose.model('buy', buySchema);