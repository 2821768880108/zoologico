'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var key = 'bchajon_1234';

exports.createToken = (user)=>{ 
    var payload = {
        sub: user._id, 
        name: user.name,
        email: user.email,
        role: user.role,
        iat: moment().unix(),
        exp: moment().add(45, "minutes").unix()
    }

    return jwt.encode(payload, key);
}