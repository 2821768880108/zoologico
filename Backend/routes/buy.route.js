
var express = require('express');
var buyController = require('../controllers/buy.controller');
var mdAuth = require('../middlewares/authenticated');

var app = express.Router();

app.post('/createBuy', mdAuth.ensureAuthAdmin, buyController.createBuy);
app.delete('/removeBuy/:id',mdAuth.ensureAuthAdmin, buyController.removeBuy);
app.get('/listBuy', buyController.listBuys);
app.put('/updateBuy/:id', mdAuth.ensureAuthAdmin, buyController.updateBuy);


module.exports = app;


