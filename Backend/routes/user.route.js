
var express = require('express');
var userController = require('../controllers/user.controller');
var mdAuth = require('../middlewares/authenticated');

var app = express.Router();
app.post('/saveUser', userController.saveUser);//crear Administrador
app.post('/createUser',mdAuth.ensureAuthAdmin, userController.createUser);//Usuario creado por un administrador
app.post('/login', userController.login);
app.delete('/removeUser/:id', mdAuth.ensureAuthAdmin, userController.removeUser);
app.get('/listUsers', mdAuth.ensureAuthAdmin, userController.getUsers);
app.put('/updateUser/:id',mdAuth.ensureAuthAdmin, userController.updateUser);

module.exports = app;