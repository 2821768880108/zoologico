
var express = require('express');
var productController = require('../controllers/product.controller');
var mdAuth = require('../middlewares/authenticated');

var app = express.Router();

app.post('/createProduct', mdAuth.ensureAuthAdmin, productController.createProduct);
app.delete('/removeProduct/:id',mdAuth.ensureAuthAdmin, productController.removeProduct);
app.get('/listProducts', productController.listProducts);
app.put('/updateProduct/:id', mdAuth.ensureAuthAdmin, productController.updateProduct);
app.get('/findProduct/:id', productController.findProduct);
app.post('/findProductbyname', productController.findProductbyname);
app.get('/findProducoutofstock', productController.findProducoutofstock);
app.get('/dataExcel', productController.dataExcel);
app.post('/viewPBS', productController.viewPBS);

module.exports = app;
