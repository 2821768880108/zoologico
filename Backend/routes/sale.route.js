
var express = require('express');
var SaleController = require('../controllers/sale.controller');
var mdAuth = require('../middlewares/authenticated');

var app = express.Router();

app.post('/createSale', mdAuth.ensureAuthAdmin, SaleController.createSale);
app.delete('/removeSaled/:id',mdAuth.ensureAuthAdmin, SaleController.removeSale);
app.get('/listSales', SaleController.listSales);
app.put('/updateSale/:id', mdAuth.ensureAuthAdmin, SaleController.updateSale);


module.exports = app;
