
var express = require('express');
var inventaryController = require('../controllers/inventary.controller');
var mdAuth = require('../middlewares/authenticated');

var app = express.Router();

app.post('/createInventary', mdAuth.ensureAuthAdmin, inventaryController.saveInventary);
/*app.delete('/removeBuy/:id',mdAuth.ensureAuthAdmin, buyController.removeBuy);
app.get('/listBuy', buyController.listBuys);
app.put('/updateBuy/:id', mdAuth.ensureAuthAdmin, buyController.updateBuy);*/


module.exports = app;


